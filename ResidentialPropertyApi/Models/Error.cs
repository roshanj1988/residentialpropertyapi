﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Models
{
    public class Error
    {
        public int Code { get; }
        public string CorrelationId { get; }
        public DateTime DateUtc { get; }
        public string ErrorMessage { get; }
        public string ErrorType { get; }

        public Error(HttpStatusCode code, string correlationId, string errorMessage, string errorType)
        {
            Code = (int)code;
            CorrelationId = correlationId;
            DateUtc = DateTime.UtcNow;
            ErrorMessage = errorMessage;
            ErrorType = errorType;
        }
    }
}
