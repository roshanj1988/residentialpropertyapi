﻿using ResidentialPropertyApi.Constants;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Filters
{
    public class CorrelationIdOperationFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
            {
                operation.Parameters = new List<IParameter>();
            }

            operation.Parameters.Add(new NonBodyParameter
            {
                Name = RequestVar.CorrelationId,
                In = "header",
                Type = "string",
                Description =
                    "Correlation id, used internally to track the request transaction. Optional will be created if not provided.",
                Required = false
            });
        }
    }
}
