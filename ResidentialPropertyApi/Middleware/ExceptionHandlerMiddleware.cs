﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using ResidentialPropertyApi.Exceptions;
using ResidentialPropertyApi.Extention;
using ResidentialPropertyApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Middleware
{
    public class ExceptionHandlerMiddleware
    {

        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception exception)
            {
                if (httpContext.Response.HasStarted) throw;

                var statusCode = ConvertExceptionToHttpStatusCode(exception);

                httpContext.Response.Clear();
                httpContext.Response.StatusCode = (int)statusCode;
                httpContext.Response.ContentType = "application/json";

                await httpContext.Response.WriteAsync(JsonConvert.SerializeObject(new Error(statusCode,
                    httpContext.Request.CorrelationId(), exception.Message, statusCode.ToSpaceString())));
            }
        }

        private HttpStatusCode ConvertExceptionToHttpStatusCode(Exception exception)
        {
            switch (exception)
            {
                case Exception ex when ex is ArgumentException:
                    return HttpStatusCode.BadRequest;
                case Exception ex when ex is ObjectNotFoundException:
                    return HttpStatusCode.NotFound;
                default:
                    return HttpStatusCode.InternalServerError;
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ExceptionHandlerMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionHandlerMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }
}
