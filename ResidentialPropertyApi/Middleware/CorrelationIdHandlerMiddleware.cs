﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using ResidentialPropertyApi.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Middleware
{
    public class CorrelationIdHandlerMiddleware
    {
        private readonly RequestDelegate _next;

        public CorrelationIdHandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext httpContext)
        {
            if (!httpContext.Request.Headers.TryGetValue(RequestVar.CorrelationId, out var correlationId) || string.IsNullOrWhiteSpace(correlationId))
            {
                correlationId = Guid.NewGuid().ToString();
                httpContext.Request.Headers[RequestVar.CorrelationId] = correlationId;
            }

            httpContext.Response.OnStarting(state =>
            {
                var context = (HttpContext)state;
                context.Response.Headers[RequestVar.CorrelationId] = correlationId;
                return Task.CompletedTask;
            }, httpContext);

            return _next(httpContext);
        }     
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class CorrelationIdHandlerMiddlewareExtensions
    {
        public static IApplicationBuilder UseCorrelationIdHandlerMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CorrelationIdHandlerMiddleware>();
        }
    }
}
