﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Constants
{
    public static class SwaggerVar
    {
        public const string Author = "Rockend Technology";
        public const string ContactUrl = "https://www.rockend.com/support";
        public const string ContactEmail = "manhattan@rockend.com";
        public const string ApiDescription = @"
    Residential Property API provides as set of endpoints to access property tree data related to property management.
";
    }
}
