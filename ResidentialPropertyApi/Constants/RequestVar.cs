﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Constants
{
    public static class RequestVar
    {
        public const string CorrelationId = "x-correlation-id";
        public const string CompanyId = "company_id";
    }
}
