﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ResidentialPropertyApi.Enrichers;
using Serilog;
using Serilog.Formatting.Json;
using Serilog.Sinks.SumoLogic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Registrations
{
    public static class SerilogRegistration
    {
        public static IServiceCollection RegisterSerilog(this IServiceCollection services, IConfiguration configuration)
        {
            var httpContextAccessor = services.BuildServiceProvider().GetService<IHttpContextAccessor>();
            var logFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configuration["serilog:write-to:RollingFile.pathFormat"]);
            var assembly = Assembly.GetExecutingAssembly();
            var location = Assembly.GetExecutingAssembly().Location;
            var fileVersion = FileVersionInfo.GetVersionInfo(location).FileVersion;

            var logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .Enrich.WithProperty("Application", assembly.GetName().Name)
                .Enrich.WithProperty("Version", fileVersion)
                .Enrich.WithProperty("Runtime", assembly.ImageRuntimeVersion)
                .Enrich.WithProperty("Schema", "1.0.0")
                .Enrich.WithMachineName()
                .Enrich.WithProcessId()
                .Enrich.WithThreadId()
                .Enrich.With(new ClientHostIpEnricher(httpContextAccessor))
                .Enrich.With(new CorrelationIdEnricher(httpContextAccessor))
                .Enrich.With(new HttpMethodEnricher(httpContextAccessor))
                .Enrich.With(new HttpRequestRawUrlEnricher(httpContextAccessor))
                .Enrich.With(new HttpStatusCodeEnricher(httpContextAccessor))
                .Enrich.FromLogContext()
                .WriteTo.RollingFile(new JsonFormatter(), logFilePath, shared: true)
                .WriteTo.SumoLogic(
                    endpointUrl: configuration["serilog:write-to:sumologic.url"],
                    sourceName: "ResidentialPropertyApi",
                    batchSizeLimit: 20,
                    period: TimeSpan.FromSeconds(1),
                    textFormatter: new JsonFormatter())
                .CreateLogger();

            services.AddSingleton<ILogger>(serviceProvider => logger);

            return services;
        }
    }
}
