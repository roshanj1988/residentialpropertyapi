﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Extention
{
    public static class HeaderDictionaryExtension
    {
        public static string AsString(this IHeaderDictionary headerDictionary)
        {
            var filteredHeaders = headerDictionary.Where(kvp => kvp.Key != "Authorization");
            return JsonConvert.SerializeObject(filteredHeaders, Formatting.Indented);
        }
    }
}
