﻿using Microsoft.AspNetCore.Http;
using ResidentialPropertyApi.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Extention
{
    public static class HttpRequestExtension
    {
        public static string CorrelationId(this HttpRequest request)
        {
            request.Headers.TryGetValue(RequestVar.CorrelationId, out var values);
            return values.FirstOrDefault();
        }

        public static string GetCompanyId(this HttpRequest request)
        {
            request.Headers.TryGetValue(RequestVar.CompanyId, out var values);
            var companyId = values.FirstOrDefault();
            if (string.IsNullOrWhiteSpace(companyId))
            {
                throw new ArgumentException($"Header: '{RequestVar.CompanyId}' not found");
            }

            return companyId;
        }
    }
}
