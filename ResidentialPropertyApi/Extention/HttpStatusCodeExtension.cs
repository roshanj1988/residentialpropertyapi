﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Extention
{
    public static class HttpStatusCodeExtension
    {
        public static string ToSpaceString(this HttpStatusCode httpStatusCode) =>
            Regex.Replace(httpStatusCode.ToString(), "([a-z])([A-Z])", "$1 $2");
    }
}
