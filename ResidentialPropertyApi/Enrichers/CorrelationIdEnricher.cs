﻿using Microsoft.AspNetCore.Http;
using ResidentialPropertyApi.Constants;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Enrichers
{
    public class CorrelationIdEnricher : ILogEventEnricher
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public CorrelationIdEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (_httpContextAccessor.HttpContext == null)
            {
                return;
            }

            var correlationId = _httpContextAccessor.HttpContext.Request.Headers[RequestVar.CorrelationId];
            var correlationIdProperty = new LogEventProperty(RequestVar.CorrelationId, new ScalarValue(correlationId));
            logEvent.AddPropertyIfAbsent(correlationIdProperty);
        }
    }
}
