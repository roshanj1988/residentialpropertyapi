﻿using Microsoft.AspNetCore.Http;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Enrichers
{
    public class HttpStatusCodeEnricher : ILogEventEnricher
    {
        private const string HttpRequestRawUrlPropertyName = "HttpStatusCode";

        private readonly IHttpContextAccessor _httpContextAccessor;

        public HttpStatusCodeEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (_httpContextAccessor.HttpContext == null)
            {
                return;
            }

            var httpResponseCode = _httpContextAccessor.HttpContext.Response.StatusCode;
            var httpResponseCodeProperty = new LogEventProperty(HttpRequestRawUrlPropertyName, new ScalarValue(httpResponseCode));
            logEvent.AddPropertyIfAbsent(httpResponseCodeProperty);
        }
    }
}
