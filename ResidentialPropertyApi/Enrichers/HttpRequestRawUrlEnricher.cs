﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Enrichers
{
    public class HttpRequestRawUrlEnricher : ILogEventEnricher
    {
        private const string HttpRequestRawUrlPropertyName = "RawUrl";

        private readonly IHttpContextAccessor _httpContextAccessor;
        public HttpRequestRawUrlEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var requestRawUrl = _httpContextAccessor.HttpContext?.Request.GetEncodedPathAndQuery();

            if (string.IsNullOrWhiteSpace(requestRawUrl))
            {
                return;
            }

            var httpRequestRawUrlProperty =
                new LogEventProperty(HttpRequestRawUrlPropertyName, new ScalarValue(requestRawUrl));
            logEvent.AddPropertyIfAbsent(httpRequestRawUrlProperty);
        }
    }
}
