﻿using Microsoft.AspNetCore.Http;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Enrichers
{
    public class ClientHostIpEnricher : ILogEventEnricher
    {
        private const string ClientIpPropertyName = "ClientIP";
        private const string HttpXForwardedFor = "HTTP_X_FORWARDED_FOR";

        private readonly IHttpContextAccessor _httpContextAccessor;
        public ClientHostIpEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (_httpContextAccessor.HttpContext == null)
            {
                return;
            }

            string userHostAddress;
            if (_httpContextAccessor.HttpContext.Request.Headers.TryGetValue(HttpXForwardedFor, out var value) &&
                !string.IsNullOrWhiteSpace(value.ToString()))
            {
                userHostAddress = value.ToString();
            }
            else
            {
                userHostAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();
            }

            if (userHostAddress.Contains(","))
                userHostAddress = userHostAddress.Split(',').First().Trim();

            var clientIpProperty = new LogEventProperty(ClientIpPropertyName, new ScalarValue(userHostAddress));
            logEvent.AddPropertyIfAbsent(clientIpProperty);
        }
    }
}
