﻿using Microsoft.AspNetCore.Http;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ResidentialPropertyApi.Enrichers
{
    public class HttpMethodEnricher : ILogEventEnricher
    {
        private const string HttpRequestTypePropertyName = "HttpMethod";

        private readonly IHttpContextAccessor _httpContextAccessor;
        public HttpMethodEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var requestMethod = _httpContextAccessor.HttpContext?.Request.Method;
            if (string.IsNullOrWhiteSpace(requestMethod))
            {
                return;
            }

            var httpRequestMethodProperty = new LogEventProperty(HttpRequestTypePropertyName, new ScalarValue(requestMethod));
            logEvent.AddPropertyIfAbsent(httpRequestMethodProperty);
        }
    }
}
